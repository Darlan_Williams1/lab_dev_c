#include <stdio.h>
#include <locale.h>

int main(){
    setlocale(LC_ALL,"Portuguese");

    char nome[50], c;
    int i;

    do{
        printf("Digite um nome: ");
        fflush(stdin);
        gets(nome);

        for (i = 0; i < nome[i] != '\0'; i++){
            switch (nome[i])
            {
            case 'z': printf("p"); break;
            case 'e': printf("o"); break;
            case 'n': printf("l"); break;
            case 'i': printf("a"); break;
            case 't': printf("r"); break;

            case 'Z': printf("P"); break;
            case 'E': printf("O"); break;
            case 'N': printf("L"); break;
            case 'I': printf("A"); break;
            case 'T': printf("R"); break;

            case 'p': printf("z"); break;
            case 'o': printf("e"); break;
            case 'l': printf("n"); break;
            case 'a': printf("i"); break;
            case 'r': printf("t"); break;
            
            case 'P': printf("Z"); break;
            case 'O': printf("E"); break;
            case 'L': printf("N"); break;
            case 'A': printf("I"); break;
            case 'R': printf("T"); break;
                 
            default: printf("%c", nome[i]); break;
            }
        }
        printf("\nContinua? (S/N): ");
        fflush(stdin);
        scanf("%c", &c);
    } while (c == 's' || c == 'S');
    
}