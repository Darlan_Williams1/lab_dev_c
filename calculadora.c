#include <stdio.h>

int main()
{
    char nome;
    int num1, num2;

    printf("digite qual a operacao: ");
    scanf("%c", &nome);

    printf("digite o primeiro numero: ");
    scanf("%d", &num1);

    printf("digite o segundo numero: ");
    scanf("%d", &num2);
    
    if(nome == '+')
    {
        int soma = num1 + num2;
        printf("o resultado da soma dos dois numeros e: %d", soma);
    }   

    else if(nome == '-')
    {
        int sub = num1 - num2;
        printf("o resultado da subtracao dos dois numeros e: %d", sub);
    }

    else if(nome == '*')
    {
        int mult = num1 * num2;
        printf("o resulta da multiplicacao dos dois numeros e: %d", mult);
    }

    else if(nome == '/')
    {
        int div = num1 / num2;
        printf("o resultado da divisao dos dois numeros e: %d", div);
    }
}