#include <stdio.h>

int main(){
    int matriz[10][2];
    int nro, lin, col;

    printf("---Digite suas notas---\n");
    for (lin = 0; lin < 10; lin++){
        for (col = 0; col < 2; col++){
            printf("Digite um numero  [%d][%d] da matriz: ", lin, col);
            scanf("%d", &matriz[lin][col]);
        }
    }
    printf("\n---Suas notas foram---\n");
    printf("nota1   nota2 \n");
    for (lin = 0; lin < 10; lin++ ){
		for(col = 0; col < 2; col++){
			printf("%d \t", matriz[lin][col]);
		}    
	printf("\n");
	}
    
    return 0;
}